Subscribem::Engine.routes.draw do
  post '/accounts', to: 'accounts#create', as: :accounts
  get '/sign_up', to: "accounts#new", as: :sign_up
  root to: 'dashboard#index'
end
